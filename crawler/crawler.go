package crawler

import (
	"./tor"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"
)

func Crawler(links []string) {
	var wg sync.WaitGroup
	wg.Add(len(links))
	for _, link := range links {
		go worker(link, &wg)
	}
	wg.Wait()
}

var regex = regexp.MustCompile("[/AZa-z0-9.]*([XAxa][XAxa][_A-Za-z0-9-]*.pdf)[.pdf]*[?][a-z0-9]*")

func worker(link string, wg *sync.WaitGroup) {
	time.Sleep(3 * time.Second)
	response, err0 := tor.HttpGet(tor.PrepareProxyClient(), link)
	if err0 != nil {
		fmt.Println("Ошибка подключения ")
		return
	}
	folder := os.Getenv("HOME") + "/journal_xaker/"
	doc, _ := goquery.NewDocumentFromResponse(response)

	hrefs := doc.Find("a.download-button").Map(
		func(i int, s *goquery.Selection) string {
			href, _ := s.Attr("href")
			return href
		})

	for _, href := range hrefs {
		name := regex.ReplaceAllString(href, "${1}")
		name = strings.Split(name, "/")[7]
		same := strings.Split(name, "_")
		fmt.Println("Файл: " + name)
		filename := folder + strings.Split(same[2], ".")[0] + "/" + same[1]
		os.MkdirAll(filename, 0777)
		filename = filename + "/" + same[0] + ".pdf"
		fmt.Println("Файл: " + filename)

		if _, err1 := os.Stat(filename); err1 == nil {
			fmt.Println("Файл уже существует")
			continue
		}

		output, err2 := os.Create(filename)
		if err2 != nil {
			fmt.Println("Ошибка создания файла, папки для скачивания не существует")
			continue
		}
		defer output.Close()

		time.Sleep(3 * time.Second)
		response, err3 := tor.HttpGet(tor.PrepareProxyClient(), href)
		if err3 != nil {
			fmt.Println("Ошибка скачивания ", err3.Error())
			os.Remove(filename)
			continue
		}
		defer response.Body.Close()

		n, err4 := io.Copy(output, response.Body)
		if err4 != nil {
			fmt.Println("Ошибка дампа ", err4.Error())
			os.Remove(filename)
			continue
		}
		fmt.Println(n)
	}
	wg.Done()
}
