package tor

import (
	"h12.me/socks"
	"log"
	"net/http"
)

func PrepareProxyClient() *http.Client {
	dialSocksProxy := socks.DialSocksProxy(socks.SOCKS5,
		"127.0.0.1:9050")

	transport := &http.Transport{Dial: dialSocksProxy}

	return &http.Client{Transport: transport}
}

func HttpGet(httpClient *http.Client, url string) (response *http.Response, err error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("User-Agent",
		`Mozilla/5.0 (X11; Linux i686)
AppleWebKit/537.36 (KHTML, like Gecko)
Chrome/36.0.1985.125 Safari/537.36`)
	response, err = httpClient.Do(req)
	return
}
